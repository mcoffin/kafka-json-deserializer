name := "kafka-json-deserializer"
version := "0.1.0"
scalaVersion := "2.12.8"

val kafkaVersion = settingKey[String]("kafka version")

kafkaVersion := "2.3.1"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % kafkaVersion.value % "provided"
libraryDependencies += "org.apache.kafka" %% "kafka" % kafkaVersion.value % "provided"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.0"
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.29"

libraryDependencies ~= { _ map(_ exclude("org.slf4j", "slf4j-log4j12")) }

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

assemblyMergeStrategy in assembly := {
	{
		case PathList("META-INF", xs @ _*) => MergeStrategy.discard
		case x => MergeStrategy.first
	}
}
