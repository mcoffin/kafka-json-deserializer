package co.mcoffin.kafka

import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.{
  Deserializer,
  StringDeserializer,
}
import play.api.libs.json._

class JsonDeserializer extends MappedDeserializer[String, JsValue](new StringDeserializer, Json.parse(_)) {
}

object JsonDeserializer {
  import scalaz._
  import scalaz.syntax.functor._
  import DeserializerInstances._
  val deserializer: Deserializer[JsValue] = (new StringDeserializer) ∘ (Json.parse(_))
}
