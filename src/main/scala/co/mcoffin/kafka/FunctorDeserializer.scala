package co.mcoffin.kafka

import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Deserializer
import scalaz.Functor

private[kafka] class MappedDeserializer[A, B] (
  val previous: Deserializer[A],
  val f: (A) => B,
) extends Deserializer[B] {
  override def configure(configs: java.util.Map[String, _], isKey: Boolean) {
    previous.configure(configs, isKey)
  }

  override def deserialize(topic: String, data: Array[Byte]): B = {
    import scala.util.{
      Try,
      Success,
      Failure,
    }
    val a = previous.deserialize(topic, data)
    Try(f(a)) match {
      case Success(v) => v
      case Failure(e: SerializationException) => {
        throw e
      }
      case Failure(e) => {
        throw new SerializationException(e)
      }
    }
  }
}

private[kafka] class FunctorDeserializer extends Functor[Deserializer] {
  override def map[A, B](fa: Deserializer[A])(f: (A) => B): Deserializer[B] = new MappedDeserializer(fa, f)
}

object DeserializerInstances {
  implicit val functorDeserializer = new FunctorDeserializer
}
